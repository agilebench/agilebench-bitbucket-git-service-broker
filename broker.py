import urllib
from brokers import BaseBroker
from django.utils import simplejson as sj

class URLOpener(urllib.FancyURLopener):
  version = 'bitbucket.org'
 
class AgileBenchBroker(BaseBroker):
  """
  Three parameters are expected within the payload['service']:
  url (required) - the AgileBench URL to which this broker will post
  token (required) - is a user's API token. Get it from your Agile Bench's user settings page.
  project_id (required) - can be found in your Agile Bench project URL. i.e. if the URL is
  http://agilebench.com/projects/3000-my-project, then the project id will be 3000.
  """
  def handle(self, payload):
    url = payload['service']['url']
    project_id = payload['service']['project_id']
    token = payload['service']['token']

    del payload['service']
    del payload['broker']

    post_load = { "payload" : payload, "data" : { "project_id" : project_id, "token" : token, "provider" : "bitbucket" } }

    # add url to the repository for every commit
    commits = []

    if 'commits' in payload and len(payload['commits']) > 0:
      for commit in payload['commits']:
        commit['url'] = 'https://bitbucket.org' + payload['repository']['absolute_url']
        commits.append(commit)
    
    payload['commits'] = commits

    #post payload to our URL
    opener = self.get_local('opener', URLOpener)
    opener.open(url, sj.dumps(post_load))


import unittest
import json
from broker import AgileBenchBroker

"""
Execute tests:
$ python tests.py


For debugging:
$ python -m pdb tests.py


Pre-reqs:
If you don't want the broker to fail to post to the service url, setup a POST
handler at http://localhost:3000/services/v1/bitbucket so you can see the output.

"""

class TestAgileBench(unittest.TestCase):

    def setUp(self):
        self.agile_bench_post = """
        {
            "broker": "twitter", 
            "commits": [
                {
                    "author": "jespern", 
                    "files": [
                        {
                            "file": "media/css/layout.css", 
                            "type": "modified"
                        }, 
                        {
                            "file": "apps/bb/views.py", 
                            "type": "modified"
                        }, 
                        {
                            "file": "templates/issues/issue.html", 
                            "type": "modified"
                        }
                    ], 
                    "message": "[#1 done] adding bump button, issue #1 fixed.", 
                    "node": "e71c63bcc05e", 
                    "revision": 1650, 
                    "size": 684
                }
            ], 
            "repository": {
                "absolute_url": "/jespern/bitbucket/", 
                "name": "bitbucket", 
                "owner": "jespern", 
                "slug": "bitbucket", 
                "website": "http://bitbucket.org/"
            }, 
            "service": {"token": "YEIFEDlTshPEoKZZqrH54SL42HbB0hR4", "project_id": "2092", "url": "http://localhost:3000/services/v1/bitbucket"}
        }
        """

    def test_posts(self):

        broker = AgileBenchBroker()

        broker.handle(json.loads(self.agile_bench_post))

        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()